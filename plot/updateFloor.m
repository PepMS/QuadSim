% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

function [] = updateFloor(parent,hfloor)
%   _____________________
% 
%   Update floor lines
%
%   []=updateFloor(parent,hfloor)
%
%   Inputs:
%       - parent:   Axes handler where to draw the floor lines.
%       - hfloor:   Floor lines handler
%
%   _____________________


xlim=get(parent,'XLim');
ylim=get(parent,'YLim');

points=[xlim(1) xlim(2) xlim(2) xlim(1) xlim(1)   0   xlim(2)  xlim(2)   0          0    0;
        ylim(1) ylim(1) ylim(2) ylim(2) 0         0      0     ylim(2)   ylim(2)    0    ylim(1);
        0       0       0       0       0         0      0     0         0          0    0];

set(hfloor,'XData',points(1,:),'YData',points(2,:),'ZData',zeros(1,size(points,2)));

return