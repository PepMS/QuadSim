% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

function [] = updateRobot(state,hquad0,hquad)
%   _____________________
% 
%   Update Robot Handler
%
%   [] = updateRobot(state,hquad0,hquad)
%
%   Inputs:
%       - state:   New position for the handler. 
%       - hquad0:  Generc Quadrotor handler at 0,0 to easy the obtantion of points.
%       - hquad:   Quadrotor handler to be updated.
%   _____________________

R = angle2dcm(state(4,1),state(5,1),state(6,1),'zyx')';

%Frame
lines_pos = get_points(hquad0(1));
hobj_pos=R*lines_pos+repmat(state(1:3,1),1,size(lines_pos,2));
set(hquad(1),'XData',hobj_pos(1,:),'YData',hobj_pos(2,:),'ZData',hobj_pos(3,:));

%Rotors
rot10=hquad0(2:4);
rot20=hquad0(5:7);
rot30=hquad0(8:10);
rot40=hquad0(11:13);
rot1=hquad(2:4);
rot2=hquad(5:7);
rot3=hquad(8:10);
rot4=hquad(11:13);

updateCylinder(rot10,rot1,state(1:3,1),R);
updateCylinder(rot20,rot2,state(1:3,1),R);
updateCylinder(rot30,rot3,state(1:3,1),R);
updateCylinder(rot40,rot4,state(1:3,1),R);

%XYZ frame
hx_p = get_points(hquad0(14));
hy_p = get_points(hquad0(15));
hz_p = get_points(hquad0(16));
hx_p=R*hx_p+repmat(state(1:3,1),1,size(hx_p,2));
hy_p=R*hy_p+repmat(state(1:3,1),1,size(hy_p,2));
hz_p=R*hz_p+repmat(state(1:3,1),1,size(hz_p,2));
set(hquad(14),'XData',hx_p(1,:),'YData',hx_p(2,:),'ZData',hx_p(3,:));
set(hquad(15),'XData',hy_p(1,:),'YData',hy_p(2,:),'ZData',hy_p(3,:));
set(hquad(16),'XData',hz_p(1,:),'YData',hz_p(2,:),'ZData',hz_p(3,:));

%In case it has a name
if size(hquad,2) == 17
    name = hquad(17);
    set(name,'Position',[hx_p(:,1)-[0.1;0.1;0.1]]);
end


hold on

return