clear
close all
% clc

%% Load Data into variables
addpath('./functions');

sensors_data = load('../simu/sensors_data.mat','a_s','w_s','range_s','t_imu','t_range','t_px4flow','px4flow_s');
eskf_imu = load('../simu/ESKF_IMU_R_OF.mat', 'xstate','tstate','P');
gtruth_data = load('../simu/gtruth.mat','gtruth','tgt');

% imu_a_b   = sensors_data.a_s;
% imu_w_b   = sensors_data.w_s;
% range_b   = sensors_data.range_s;
% flow_b    = sensors_data.px4flow_s(1:3, :);
% t_imu_b   = sensors_data.t_imu;
% t_range_b = sensors_data.t_range;
% t_flow_b  = sensors_data.t_px4flow;

gtruth = gtruth_data.gtruth;
tgt = gtruth_data.tgt;


imu_data = load('../simu/sensors/imu.mat','imu');
range_data = load('../simu/sensors/range.mat','range');
flow_data = load('../simu/sensors/px4flow.mat','px4flow');

t_imu   = imu_data.imu(:,1)';
imu_a = imu_data.imu(:,2:4)';
imu_w = imu_data.imu(:,5:7)';
t_range = range_data.range(:,1)';
range = range_data.range(:,2)';
t_flow  = flow_data.px4flow(:,1)';
flow    = flow_data.px4flow(:, [2,6:7])';

% Comparison
xstate = eskf_imu.xstate;
tstate = eskf_imu.tstate;

%% Filter parameters and containers
N = 19; % Number of nominal states

% Other parameters
gravity = 9.81;
gv      = [0; 0; -gravity];

%% Sensor parameters

% Noise - Estimation sensor
noise_gyro       = 0.05;
noise_gyro_bias  = 0.00;
noise_accel      = 0.05;
noise_accel_bias = 0.00;

% Noise - Correction sensors
noise_range     = 0.01;
noise_acc_grav  = 100;
noise_of        = 0.1;

%% Initialization

% Initial nominal states
p0  = [0; 0 ;0.001];
v0  = [0; 0; -0.0001];
q0  = e2q([0,0,0]);
ab0 = ones(3,1)*0.00;
wb0 = ones(3,1)*0.00;
g0 = gv;

x = [p0; v0; q0; ab0; wb0; g0]; % Initial state
x(1:10) = gtruth(1:10, 1);
std_ini = [0, 0, 0, 0, 0, 0, 0.0500, 0.0500, 0, 0.0500, 0.0500, 0.0500, 0.0500, 0.0500, 0, 0, 0, 0];
P = diag(std_ini.^2);

XX(:, 1)    = x;
PP(:, :, 1) = P;
TT = t_imu(1);

idx_imu   = 2;
idx_range = 1;
idx_flow  = 1;

sensors = {};

while idx_imu <= size(t_imu, 2)
    dt = t_imu(idx_imu) - t_imu(idx_imu-1);
    
    dx = zeros(N-1,1);
    
    ang_vel = imu_w(:, idx_imu) - x(14:16);
    
    sensor_names = {'imu'};
    sensor_times = [t_imu(idx_imu)];
    if idx_flow < size(t_flow,2)
        sensor_names(end+1) = {'flow'};
        sensor_times = [sensor_times t_flow(idx_flow)];
    end
    if idx_range < size(t_range,2)
        sensor_names(end+1) = {'range'};
        sensor_times = [sensor_times t_range(idx_range)];
    end
    [~,M] = min(sensor_times);
    [sensor] =  sensor_names{M};
    sensors(end+1) = {sensor};
    
    switch sensor
        case 'imu'
            % -----------IMU integration--------------

            a = imu_a(:, idx_imu-1);
            w = imu_w(:, idx_imu-1);
            
            % Nominal-State update
            [p, v, q, ab, wb, g, qw, qv] = assignState(x);
            
            q_nom  = qNorm(qProd(q,vec2q((w-wb)*dt)));
            %             q_nom    = [1;(w-wb)*dt/2];
            %             q_nom = leftQuaternion(q)*q_nom;
            %             q_nom = q_nom/norm(q_nom);
            R = q2R(q_nom);
            aux = R*(a-ab) + g;
            
            p_nom   = p + v*dt;
            v_nom   = v + aux*dt;
            ab_nom = ab;
            wb_nom = wb;
            
            x = [p_nom; v_nom; q_nom; ab_nom; wb_nom; g];
            % Covar. Propagation - Error-State Jacobian
            
            [p, v, q, ab, wb, g, qw, qv] = assignState(x);
                        
            a = imu_a(:, idx_imu);
            w = imu_w(:, idx_imu);
            
            %             R = q2R(q);
            %             Theta = -vec2skew(w-wb);
            %             V  = -R*vec2skew(a-ab);
            %
            %             A_dx = ...
            %                 [zeros(3), eye(3),   zeros(3), zeros(3), zeros(3);...
            %                 zeros(3), zeros(3), V       , -R      , zeros(3);...
            %                 zeros(3), zeros(3), Theta   , zeros(3), -eye(3);  ...
            %                 zeros(6,N-1)];
            %
            %             F_dx = eye(N-1) + A_dx*dt;
            R = q2R(q);
            Rw = vec2R((w-wb)*dt);
            
            Pv = eye(3);
            Va=-R;
            Vg=eye(3);
            
            sk_a = vec2skew(a-ab);
            Vtheta=-R*sk_a;
            Thetatheta=vec2skew(w-wb);
            Trunc_sigma0=Rw';
            Thetaomega=-eye(3);
            
            Trunc_sigma1 = eye(3)*dt;
            Trunc_sigma2 = zeros(3);
            Trunc_sigma3 = zeros(3);
            
            F_dx = [eye(3)    Pv*dt       Pv*Vtheta*Trunc_sigma2      Pv*Va*Trunc_sigma2      Pv*Vtheta*Trunc_sigma3*Thetaomega   0.5*Pv*Vg*dt^2;
                zeros(3)  eye(3)      Vtheta*Trunc_sigma1         Va*dt               Vtheta*Trunc_sigma2*Thetaomega      Vg*dt;
                zeros(3)  zeros(3)    Trunc_sigma0                zeros(3)            Trunc_sigma1*Thetaomega             zeros(3);
                zeros(3)  zeros(3)    zeros(3)                    eye(3)              zeros(3)                            zeros(3);
                zeros(3)  zeros(3)    zeros(3)                    zeros(3)            eye(3)                              zeros(3);
                zeros(3)  zeros(3)    zeros(3)                    zeros(3)            zeros(3)                            eye(3)];
            
            % Covariance matrix
            V_i     = noise_accel^2*dt^2*eye(3);
            Theta_i = noise_gyro^2*dt^2*eye(3);
            A_i     = noise_accel_bias^2*dt*eye(3);
            Omega_i = noise_gyro_bias^2*dt*eye(3);
            
            Q_i  = blkdiag(V_i, Theta_i, A_i, Omega_i);
            F_i = [zeros(3,12); eye(12); zeros(3, 12)];
            
            % State update
            P = F_dx*P*F_dx' + F_i*Q_i*F_i';
            P = (P + P')/2;
            
            %             --- ACCELEROMETER ---
            %             H_x = 2*[-qv(2)    qv(3)  -qw    qv(1); ...
            %                 qv(1)    qw     qv(3)  qv(2); ...
            %                 qw      -qv(1)  -qv(2) qv(3)];
            %             % H_x = [zeros(3, 6), H_x, zeros(3, 6)];
            %             H_x = [zeros(3, 6), H_x*gravity, zeros(3, 6)];
            %             X_dx = Qmat(q);
            %             X_dx = blkdiag(eye(6), X_dx, eye(6));
            %             H_dx = H_x*X_dx;
            %
            %             Na = noise_acc_grav^2*eye(3);
            %             Z = H_dx*P*H_dx' + Na;
            %             Z = (Z + Z)'/2;
            %             K = P*H_dx'/Z;
            %
            %             R = fromqtoR(q);
            %             a_est = -R'*gv;
            %             z = (a-ab) - a_est;
            %             dx = K*z;
            %             P = P - K*Z*K';
            %             P = (P + P')/2;
            %             S = eye(N-1) - K*H_dx;
            %             P = S*P*S' + K*Z*K';
            
            % Reset operation
            %             x(1:3)   = p + dx(1:3);
            %             x(4:6)   = v + dx(4:6);
            %             qe = vec2q(dx(7:9));
            %             x(7:10) = qNorm(qProd(q,qe));
            %             % x(7:10)  = leftQuaternion(q)*[1; dx(7:9)/2];
            %             %                 x(7:10)  = leftQuaternion(q)*q_aux;
            %             %                 x(7:10)  = x(7:10)/norm(x(7:10));
            %             x(11:13) = ab + dx(10:12);
            %             x(14:16) = wb + dx(13:15);
            %             dx = zeros(N-1,1);
            
            TT(end + 1) = t_imu(idx_imu);
            idx_imu = idx_imu + 1;
            XX(:, end + 1) = x;
            PP(:, :, end +1) = P;
            
            if idx_imu == 2500
                P;
            end
            
        case 'range'
            r = range(idx_range);
            if idx_imu >=3 && r > 0.3 && r < 4.0
                if (r< 0)
                    r = 0;
                end
                
                [p, v, q, ab, wb, g, qw, qv] = assignState(x);
                
                [qc,QC_q] = q2qc(q);
                R_trans = q2R(qc);
                Sz = [0 0 1];
                h = Sz*R_trans*p;
                
                Hr = Sz*vec2skew(R_trans*p);
                
                Hp = Sz*R_trans;
                
                H_dx = [Hp zeros(1,3) Hr zeros(1,9)];
                
                Nr = 0.0025;
                Z = H_dx*P*H_dx' + Nr;
                Z = (Z+Z')/2;
                K = P*H_dx'/Z;
                
                z = r - h;
                dx = K*z;
                S = eye(N-1) - K*H_dx;
                P = P - K*Z*K';
                P = (P + P')/2;
                
                % Reset operation
                x(1:3)   = p + dx(1:3);
                x(4:6)   = v + dx(4:6);
                qe = vec2q(dx(7:9));
                x(7:10) = qNorm(qProd(q,qe));
                x(11:13) = ab + dx(10:12);
                x(14:16) = wb + dx(13:15);
                x(17:19) = g  + dx(16:18);
                dx = zeros(N-1,1);
                
                TT(end + 1) = t_range(idx_range);
                XX(:, end + 1) = x;
                PP(:, :, end +1) = P;
                
            end
            idx_range = idx_range + 1;
        case 'flow'
            f_xy = flow(:, idx_flow);
            if idx_imu >=3 && f_xy(1) > 0.3 && f_xy(1) < 4.0
                
                [p, v, q, ab, wb, g qw, qv] = assignState(x);
                
                [qc,QC_q] = q2qc(q);
                [~, W_v, W_qc] = qRot(v,qc);
                W_q =  W_qc * QC_q;
                R_trans = q2R(qc);
                
                Sxy = [eye(2,2) zeros(2,1)];
                Sz = [0 0 1];
                
                if (p(3) <= 0.0)
                    p(3) = 1e-3;
                end
                
                % Observation model
                f = 500;
                w = ang_vel;
                wyx = [-w(2,1);w(1,1)];
                z = Sz*p;
                
                h = [Sz*p;-(f*Sxy*R_trans*v)/z + f*wyx];
                
                Hz = f*Sxy*R_trans*v/(z^2); % Avoided due to denominator
                Hv = -(f*Sxy*R_trans)/z;
                term_rot = vec2skew(R_trans*v);
                
                Hr = -(f*Sxy*term_rot)/z;
                
                H_dx = [ Sz              zeros(1,3)  zeros(1,3)  zeros(1,9);
                    [zeros(2,2) Hz]  Hv          Hr          zeros(2,9)];
                
                Nr = diag([0.0025 625 625]);
                Z = H_dx*P*H_dx' + Nr;
                Z = (Z + Z')/2;
                K = P*H_dx'/Z;
                
                
                z = f_xy - h;
                dx = K*z;
                S = eye(N-1) - K*H_dx;
                P = P - K*Z*K';
                P = (P + P')/2;
%                 P = S*P*S' + K*Z*K';
%                 P = (P + P')/2;
                
                % Reset operation
                x(1:3)   = p + dx(1:3);
                x(4:6)   = v + dx(4:6);
                qe = vec2q(dx(7:9));
                x(7:10) = qNorm(qProd(q,qe));
                x(11:13) = ab + dx(10:12);
                x(14:16) = wb + dx(13:15);
                x(17:19) = g  + dx(16:18);
                dx = zeros(N-1,1);
                
                TT(end + 1) = t_flow(idx_flow);
                XX(:, end + 1) = x;
                PP(:, :, end +1) = P;
            end
            idx_flow = idx_flow + 1;
    end
end

%% Plot position
figure;
subplot(3,1,1)
plot(TT, XX(1, :), tstate, xstate(1,:),  tgt, gtruth(1,:))
title('X position');

subplot(3,1,2)
plot(TT, XX(2, :), tstate, xstate(2,:), tgt, gtruth(2,:))
title('Y position');

subplot(3,1,3)
plot( TT, XX(3, :), tstate, xstate(3,:),tgt, gtruth(3,:))
title('Z position');

%% Plot velocity
figure;
subplot(3,1,1)
plot(TT, XX(4, :), tstate, xstate(4,:),  tgt, gtruth(4,:))
title('X vel');

subplot(3,1,2)
plot(TT, XX(5, :), tstate, xstate(5,:),  tgt, gtruth(5,:))
title('Y vel');

subplot(3,1,3)
plot(TT, XX(6, :), tstate, xstate(6,:),  tgt, gtruth(6,:))
title('Z vel');


%% Plot orientation
RPY = zeros(3,length(TT));

RPY_f = zeros(3,length(tstate));
RPY_t = zeros(3,length(tgt));

for ii = 1:length(TT)
    RPY(:, ii) = q2e(XX(7:10, ii));
end

for ii = 1:length(tstate)
    RPY_f(:, ii) = q2e(xstate(7:10, ii));
end

for ii = 1:length(tgt)
    RPY_t(:, ii) = q2e(gtruth(7:10, ii));
end

figure;
subplot(3,1,1)
plot(TT, RPY(1,:), tstate, RPY_f(1,:), tgt, RPY_t(1,:))
title('Roll');

subplot(3,1,2)
plot(TT, RPY(2,:), tstate, RPY_f(2,:), tgt, RPY_t(2,:))
title('Pitch');

subplot(3,1,3)
plot(TT, RPY(3,:), tstate, RPY_f(3,:), tgt, RPY_t(3,:))
title('Yaw');