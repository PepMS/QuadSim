clear
close all
clc

%% Load Data into variables
addpath('./functions');

sensors_data = load('../simu/sensors_data.mat','a_s','w_s','range_s');
eskf_imu = load('../simu/ESKF_IMU_R.mat', 'xstate','tstate');

imu_a = sensors_data.a_s;
imu_w = sensors_data.w_s;
range = sensors_data.range_s;
% px4flow = px4flow_data.px4flow(:, [2,6:7]);

dt = 0.01;
t  = 0:dt:(size(imu_a,1) - 1)*dt;

% Comparison
xstate = eskf_imu.xstate;
tstate = eskf_imu.tstate;

%% Filter parameters and containers
N = 16; % Number of nominal states

XX = zeros(N, size(t, 2)); % State container
PP = zeros(N-1, N-1, size(t, 2)); % Covariance container

% Other parameters
gravity = 9.81;
gv      = [0; 0; -gravity];

%% Sensor parameters

% Noise - Estimation sensor
noise_gyro       = 0.05;
noise_gyro_bias  = 0.00;
noise_accel      = 0.05;
noise_accel_bias = 0.00;

% Noise - Correction sensors
noise_range     = 0.01;
noise_acc_grav  = 100;
noise_of        = 0.1;

%% Initialization

% Initial nominal states
p0  = [0; 0 ;0];
v0  = [0; 0; 0];
q0  = e2q([0,0,0]);
ab0 = ones(3,1)*0.00;
wb0 = ones(3,1)*0.00;

x = [p0; v0; q0; ab0; wb0]; % Initial state
std_ini = [0, 0, 0, 0, 0, 0, 0.0500, 0.0500, 0, 0.0500, 0.0500, 0.0500, 0.0500, 0.0500, 0];
P = diag(std_ini.^2);

XX(:, 1)   = x;
PP(:, :, 1) = P;

for ii = 1:length(t)
    
    a = imu_a(:, ii);
    w = imu_w(:, ii);
    
    r = range(ii);
    
    % -----------IMU integration--------------
    % Nominal-State update
    [p, v, q, ab, wb, qw, qv] = assignState(x);
    q  = qNorm(qProd(q,vec2q((w-wb)*dt)));
    x(7:10) = q;
    R = q2R(q);
    aux      = R*(a-ab) + gv;
    
    x(1:3)   = p + v*dt;
    x(4:6)   = v + aux*dt;
    x(11:13) = ab;
    x(14:16) = wb;
    
    [p, v, q, ab, wb, qw, qv] = assignState(x);
    
    % Error-State Jacobian
    R = fromqtoR(q);
    Theta = -skew(w-wb);
    V  = -R*skew(a-ab);
    
    A_dx = ...
        [zeros(3), eye(3),   zeros(3), zeros(3), zeros(3);...
        zeros(3), zeros(3), V       , -R      , zeros(3);...
        zeros(3), zeros(3), Theta   , zeros(3), -eye(3);  ...
        zeros(6,N-1)];
    
    F_dx = eye(N-1) + A_dx*dt;
    
    % Covariance matrix
    V_i     = noise_accel^2*dt^2*eye(3);
    Theta_i = noise_gyro^2*dt*eye(3);
    A_i     = noise_accel_bias^2*dt^2*eye(3);
    Omega_i = noise_gyro_bias^2*dt*eye(3);
    
    Q_i  = blkdiag(V_i, Theta_i, A_i, Omega_i);
    F_i = [zeros(3,12); eye(12)];
    
    % State update
    P = F_dx*P*F_dx' + F_i*Q_i*F_i';
    
    % ------------Correction
    
    % --- ACCELEROMETER ---
%         H_x = 2*[-qv(2)    qv(3)  -qw    qv(1); ...
%                   qv(1)    qw     qv(3)  qv(2); ...
%                   qw      -qv(1)  -qv(2) qv(3)];
%         % H_x = [zeros(3, 6), H_x, zeros(3, 6)];
%         H_x = [zeros(3, 6), H_x*gravity, zeros(3, 6)];
%         X_dx = Qmat(q);
%         X_dx = blkdiag(eye(6), X_dx, eye(6));
%         H_dx = H_x*X_dx;
%     
%         Na = noise_acc_grav^2*eye(3);
%         Z = H_dx*P*H_dx' + Na;
%         Z = (Z + Z)'/2;
%         K = P*H_dx'/Z;
%     
%         R = fromqtoR(q);
%         a_est = -R'*gv;
%         z = (a-ab) - a_est;
%         dx = K*z;
%         S = eye(N-1) - K*H_dx;
%         P = S*P*S' + K*Z*K';
%     
%         % Reset operation
%         x(1:3)   = p + dx(1:3);
%         x(4:6)   = v + dx(4:6);
%         x(7:10)  = leftQuaternion(q)*[1; dx(7:9)/2];
%         x(7:10)  = x(7:10)/norm(x(7:10));
%         x(11:13) = ab + dx(10:12);
%         x(14:16) = wb + dx(13:15);
%     
%         dx = zeros(N-1,1);
%         P = (P+P')/2;
    
    % --- RANGE FINDER ---
    [p, v, q, ab, wb, qw, qv] = assignState(x);
    
    [qc,QC_q] = q2qc(q);
    R_trans = q2R(qc);
    Sz = [0 0 1];
    h = Sz*R_trans*p;
    
    Hr = Sz*vec2skew(R_trans*p);

    Hp = Sz*R_trans;
           
    H_dx = [Hp zeros(1,3) Hr zeros(1,6)];
    
    Nr = 0.0025;
    Z = H_dx*P*H_dx' + Nr;
    K = P*H_dx'/Z;
    
    z = r - h;
    dx = K*z;
    S = eye(N-1) - K*H_dx;
    P = S*P*S' + K*Z*K';
    P = (P + P')/2;
    
    % Reset operation
    x(1:3)   = p + dx(1:3);
    x(4:6)   = v + dx(4:6);
    x(7:10)  = leftQuaternion(q)*[1; dx(7:9)/2];
    x(7:10)  = x(7:10)/norm(x(7:10));
    x(11:13) = ab + dx(10:12);
    x(14:16) = wb + dx(13:15);
    dx = zeros(N-1,1);
    
    % --- OPTICAL FLOW ---
%     [p, v, q, ab, wb, qw, qv] = assignState(x);
%     
%     [qc,QC_q] = q2qc(q);
%     [~, W_v, W_qc] = qRot(v,qc);
%     W_q =  W_qc * QC_q;
%     R_trans = q2R(qc);
%     
%     Sxy = [eye(2,2) zeros(2,1)];
%     Sz = [0 0 1];
%     
%     if (p(3) <= 0.0)
%         p(3) = 1e-3;
%     end
%     
%     % Observation model
%     f = 500;
%     w = w-wb;
%     wyx = [-w(2,1);w(1,1)];
%     z = Sz*p;
%     
%     h = [Sz*p;-(f*Sxy*R_trans*v)/z + f*wyx];
% 
%     Hz = f*Sxy*R_trans*v/(z^2); % Avoided due to denominator
%     Hv = -(f*Sxy*R_trans)/z;
%     term_rot = vec2skew(R_trans*v);
%     
%     Hr = -(f*Sxy*term_rot)/z;
% 
%     H_dx = [ Sz              zeros(1,3)  zeros(1,3)  zeros(1,6);
%         [zeros(2,2) Hz]  Hv          Hr          zeros(2,6)];
%        
%     Nr = diag([0.0025 625 625]);
%     Z = H_dx*P*H_dx' + Nr;
%     Z = (Z + Z')/2;
%     K = P*H_dx'/Z;
%     
%     y = px4flow(ii, :)';
%     
%     z = y - h;
%     dx = K*z;
%     S = eye(N-1) - K*H_dx;
%     P = S*P*S' + K*Z*K';
%     P = (P + P')/2;
%     
%     % Reset operation
%     x(1:3)   = p + dx(1:3);
%     x(4:6)   = v + dx(4:6);
%     x(7:10)  = leftQuaternion(q)*[1; dx(7:9)/2];
%     x(7:10)  = x(7:10)/norm(x(7:10));
%     x(11:13) = ab + dx(10:12);
%     x(14:16) = wb + dx(13:15);
%     dx = zeros(N-1,1);
    
    % Fill containers
    XX(:, ii) = x;
    PP(:, :, ii) = P;
    
end

%% Plot position
figure;
subplot(3,1,1)
%plot(t, XX(1, :), t, gtruth(:, 2))
% plot(t, xstate(2,:), t, gtruth(:, 2))
plot(tstate, xstate(1,:), t, XX(1, :))
title('X position');

subplot(3,1,2)
%plot(t, XX(2, :), t, gtruth(:, 3))
%plot(t, xstate(2,:), t, gtruth(:, 3))
plot(tstate, xstate(2,:), t, XX(2, :))
title('Y position');

subplot(3,1,3)
%plot(t, XX(3, :), t, gtruth(:, 4))
% plot(t, xstate(4,:), t, gtruth(:, 4))
plot(tstate, xstate(3,:), t, XX(3, :))
title('Z position');

%% Plot orientation
RPY = zeros(3,length(t));

RPY_t = zeros(3,length(tstate));

for ii = 1:length(t)
    RPY(:, ii) = q2e(XX(7:10, ii));
end

for ii = 1:length(tstate)
    RPY_t(:, ii) = q2e(xstate(7:10, ii));
end

figure;
subplot(3,1,1)
plot(t, RPY(1,:), tstate, RPY_t(1,:))
title('Roll');

subplot(3,1,2)
plot(t, RPY(2,:), tstate, RPY_t(2,:))
title('Pitch');

subplot(3,1,3)
plot(t, RPY(3,:), tstate, RPY_t(3,:))
title('Yaw');