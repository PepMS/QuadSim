function [R, P, Y] =  q2RPY(q)
qw = q(1);
qx = q(2);
qy = q(3);
qz = q(4);

% Roll
sinr_cosp = +2.0 * (qw * qx + qy * qz);
cosr_cosp = +1.0 - 2.0 * (qx^2 + qy^2);
R = atan2(sinr_cosp, cosr_cosp);

% Pitch
sinp = +2.0 * (qw * qy - qz * qx);
if (abs(sinp) >= 1)
    P = sign(sinp)*pi/2; % use 90 degrees
else
    P = asin(sinp);
end

% Yaw
siny_cosp = +2.0 * (qw * qz + qx * qy);
cosy_cosp = +1.0 - 2.0 * (qy^2 + qz^2);
Y = atan2(siny_cosp, cosy_cosp);

end