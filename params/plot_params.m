% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

%% PLOT_PARAMS 
%
% PLOT_PARAMS is a script that creates the workspace variable plot with 
% the required parameters for the simulink block quadrotor_plot.
% It increments the variables params.

params.plot.font_size = 15;
params.plot.view.az = -50;
params.plot.view.el = 20;
params.plot.scene_size = [-3 3 -3 3 0 3];
params.plot.scene_center = [0;0;0]; % [x;y;z]
params.plot.frame_length = 0.1;
params.plot.robot.quad.params.color = [0 0 0];
params.plot.robot.quad.params.size_m2m = 0.4;
params.plot.robot.quad.params.odom.color = [0 0 1];
params.plot.robot.quad.params.odom.style = '-.';
