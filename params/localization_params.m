% Copyright (C) by P. Martí-Saumell (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.

%% Localization_params

% Contains the parameters needed by the localization & estimation algorithm
% selected

% Main definitions
filter_type = {'eskf','ekf','graph_slam'};   
ori_error = {'global','local'};
qint = {'zerof','zerob','first'};
imu_trunc = {1,2,3};

params.localization.type      = filter_type{1};
params.localization.ori_error = ori_error{2};
params.localization.qint_met  = qint{1};
params.localization.imu_met  = imu_trunc{1};
